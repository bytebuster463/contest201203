﻿module Bytebuster.Common.FSharp.Xml

open System.Xml.Linq

let xn (tag:string) = XName.Get(tag)
//let xname n = XName.op_Implicit(n)
let xdoc (el:seq<XElement>) = new XDocument(Array.map box (Array.ofSeq el))
let xelem s el = new XElement(xn s, box el)
let xatt a b = new XAttribute(xn a, b) |> box
let xstr s = box s