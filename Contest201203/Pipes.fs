﻿module Bytebuster.Common.FSharp.Pipes

/// http://tryfs.net/snippets/snippet-4o
/// High precedence, right associative backward pipe
/// Usage:
/// Seq.toList ^<| Seq.map (fun x -> x + 3) ^<| {1..10}
/// Instead of:
/// Seq.toList <| (Seq.map (fun x -> x + 3) <| {1..10})

let inline (^<|) f a = f a

