﻿module Bytebuster.Common.FSharp.Option

/// A simple ternary. Use like this: let x3 = true |> 2 ^? 3
let inline (^?) (a:'T) (b:'T) condition :'T = if condition then a else b

/// A simple ternary for wrapping Option. Use like this: List.Pick(fun (x:bool, y) -> x =? y)
//let inline (^!?) condition (a:'T) :'T option = if condition then Some a else None
let inline (=?) condition (a:'T) :'T option = if condition then Some a else None

/// A simple ternary for wrapping Option. Use like this: List.tryPick(fun (x:bool, y: 'T option) -> x ^!?? y)
let inline (^!??) condition (a:'T option) :'T option = if condition then a else None

/// A simple ternary for unwrapping Option. Use like this (s:Option<'T>, t:'T, x:'T): let x = s ^?? t
let inline (^??) (a: Option<'T>) (defValue: 'T) =
    match a with
    | None -> defValue
    | Some x -> x

/// A simple ternary for unwrapping Option. Use like this (q:Option<'T>, p:'T->'U, p2:'U): let x = q ^?> p1 <| p2
let inline (^?>) (q: Option<'T>) (p1: 'T -> 'U) (p2: 'U) =
    match q with
    | Some x -> p1 x
    | None   -> p2

let inline (^?>?) (q: Option<'T>) (p1: 'T -> 'U) =
    q |> Option.bind (p1 >> Some)

