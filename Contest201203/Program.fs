﻿//[<EntryPoint>]
module Bytebuster.Contest201203.Program

open System
open System.Xml.Linq
open System.Diagnostics
open Bytebuster.Common.FSharp.Pipes
open Bytebuster.Common.FSharp.Xml

// State definition
/// Defines state of a single tank
type State0 = int           // shouldn't be necessarily int
/// Defines state of full set of tanks
type State = State0 list

///[<Measure>] type tank
type Pos = int // <tank> // too lazy to implement type-safe indexers

/// Action denotes types of possible moves
type Action =
    /// Initial state; Used when for uniform representation (and if initial state is winning at once)
    | Start
    /// Filling up a tank number Pos; RULE: Filling up is always done till the tank reaches its capacity
    | FillUp    of Pos
    /// Emptying a tank number Pos; RULE: There is no partial emtying
    | Empty     of Pos
    /// Refilling from a tank Pos1 to tank Pos2; RULE: amount of refilling equals to current amount in Tank1 OR remaining space in Tank2, whichever is less
    | Refill    of Pos * Pos
    with
    override this.ToString() =
        match this with
        | Start         -> sprintf "Initial state"
        | FillUp x      -> sprintf "Fill up %d" x
        | Empty  x      -> sprintf "Empty %d" x
        | Refill (x, y) -> sprintf "Refill from %d to %d" x y

/// Functional alias for printing Action
let printAction act = act.ToString()

type HistoryItem    = Action * State
type History        = HistoryItem list
type Solution       = History list

/// type alias for state validator
type StateValidPredicate = State -> bool

/// Returns whether the state is NOT already stored in history 
let isStateNew history state =
    history
    |> List.forall ^<| (snd >> (<>) state)

// A predicate returning state validator.
// Validator checks if state is expected (winning) position
let isFinalHelper (requiredState: State0) (tanks: Pos) (* : StateValidPredicate *) =
    if tanks < 1 then failwith "Tanks number must be positive"
    fun (state: State) ->
        if not (state.Length = tanks) then failwithf "Expecting State[%d] but given State[%d]" tanks state.Length
        [ for index=0 to tanks-1 do yield fun (x: State) -> x.[index] = requiredState ] // a predicate comparing state of a single tank
        |> List.exists ^<| fun x -> x state

/// Action generator
/// It produces all possible Actions (moves) for a defined number of tanks
let allMoves (tanks: Pos) =
    // No idea how to optimize it
    let listInit2D length pred =
        seq { for i in 0..length-1 do for j in 0..length-1 do if i <> j then yield pred(i, j) }
        |> Seq.toList

    [
        List.init tanks Empty;
        listInit2D tanks Refill;
        List.init tanks FillUp;
    ]
    |> List.concat

/// Determines if a given Action is applicable in current context
/// A context if defined by tank capacities and current state of all tanks
/// RULE: cannot FillUp a full tank (e.g. state = capacity)
/// RULE: cannot Refill to a full tank (e.g. state = capacity)
/// RULE: cannot Empty an empty tank
/// RULE: cannot Refill from an empty tank
let isApplicable (capacities: State) (state: State) (action: Action) =
    let IsNotFull  x = state.[x] <> capacities.[x]
    let IsNotEmpty x = state.[x] <> 0
    match action with
    | FillUp x      -> IsNotFull x
    | Empty x       -> IsNotEmpty x
    | Refill(x, y)  -> IsNotEmpty x && IsNotFull y
    | _             -> false

/// Applies an action and returns an updated state of tanks set
/// Obeys rules for refilling
/// RULE: can refill no more than contains in a "from" tank and no more than "to" tank has available space
let applyMove (capacities: State) (initial: State) (action: Action) : (Action * State) =
    let z (initial: State) = function
        | FillUp x      -> initial |> List.mapi ^<| fun i el -> if x = i then capacities.[i] else el
        | Empty x       -> initial |> List.mapi ^<| fun i el -> if x = i then 0 else el
        | Refill(x, y)  ->
                            let difference = min initial.[x] (capacities.[y] - initial.[y]) // how much can refill
                            initial |> List.mapi ^<| fun i el -> if i=x then el-difference elif i=y then el+difference else el
        | _             -> initial
        
    action, z initial action

/// Performs a generic step
/// Gets a full list of all possible solutions for current state
/// NOTE: recursion occurs by depth levels.
/// E.g., first it finds 1-step solutions; if found, returns a winner;
/// otherwise it generates a list of possible 2-step solutions and invokes next cycle
/// Is tail-recursive
let rec move (possibleMoves: Action list) isApplicable2 applicationPredicate isWinningState (candidates: Solution) : Solution =
    let outcomes  =
        candidates
        |> List.map ^<| fun candidate ->
            match candidate with
            | []    -> failwith "Internal Error, empty history is unexpected here"
            | state::_  ->
                if state |> isWinningState
                then [candidate]                                        // WIN; single item
                else
                    possibleMoves                                       // All possible moves
                    |> List.filter ^<| isApplicable2 state              // filtered only those producing meaningful result
                    |> List.map ^<| applicationPredicate state          // Apply action and get new state
                    |> List.filter ^<| (snd >> isStateNew candidate)    // Filtered only those producing state never met before
                    |> List.map ^<| fun x -> (x::candidate)             // Append element to history
        |> List.concat
    let winners : Solution =
        outcomes
        |> List.filter (List.head >> isWinningState)
    if not winners.IsEmpty then winners                                 // WINNER
    elif outcomes.IsEmpty then []                                       // IMPOSSIBLE
    else
        move possibleMoves isApplicable2 applicationPredicate isWinningState outcomes // CONTINUE

/// Gets a full list of all possible solutions for current state
let AllSolutions(initial: State) (capacities: State) (expected: State0): Solution =
    if capacities.IsEmpty then raise (ArgumentException("Expecting non-zero Tanks list", "capacities"))
    if initial.IsEmpty then raise (ArgumentException("Expecting non-zero Tanks list", "initial"))
    if initial.Length <> capacities.Length then raise (ArgumentException(sprintf "Expecting initial state and capacities to have equal length, but given initial[%d], capacities[%d]" initial.Length capacities.Length))
    capacities |> List.iteri ^<| fun i x -> if x < 0 then raise(ArgumentOutOfRangeException("initial", (sprintf "Expecting positive capacities but given capacities[%d]=%d" i x)))
    initial |> List.iteri ^<| fun i x -> if x < 0 then raise(ArgumentOutOfRangeException("capacities", (sprintf "Expecting positive initial state but given initial[%d]=%d" i x)))

    // Solution-wide computed values
    let numberOfTanks = initial.Length
    let isWinningState = snd >> (isFinalHelper expected numberOfTanks)
    let possibleMoves = allMoves capacities.Length
    let applyMove2 = snd >> applyMove capacities
    let isApplicable2 = snd >> isApplicable capacities

    move possibleMoves isApplicable2 applyMove2 isWinningState [[Start, initial]]

/// Calculates the list of minimal contest solutions
/// Main entry point for contest
let BestSolutions initial capacities expected =
    let all = AllSolutions initial capacities expected                  // take all possible solutions (pre-filtering already occurred)
    if all.IsEmpty then []                                              // no solution
    else
        let minimal = all |> List.minBy List.length                     // compute the smallest solution length
        all |> List.filter ^<| (List.length >> ((=) minimal.Length))    // filter solutions list in case if larger values occurred
    
/// Printing a single Action to Xml
let XmlFromAction action =
    xelem "action" (
        match action with
        | Start         -> [xatt "type" "Initial state"]
        | FillUp x      -> [xatt "type" "Fill up"; xatt "tank" x]
        | Empty  x      -> [xatt "type" "Empty"; xatt "tank" x]
        | Refill (x, y) -> [xatt "type" "Refill"; xatt "from" x; xatt "to" y]
    )

/// Printing a single History Item to Xml
let XmlFromHistoryItem historyItem =
    let action, state = historyItem
    xelem "step" [
        XmlFromAction action
        xelem "state" (
            state
            |> List.map ^<| fun el -> el.ToString()
            |> String.concat "; "
        )
    ]

/// Printing entire history to Xml
let XmlFromHistory (history: History) =
    xelem "steps" [
        history
        |> List.rev
        |> List.map XmlFromHistoryItem
    ]

/// A dummy printing facility
let printz x =
    printf "%s" x
    Debug.WriteLine x

/// Printing entire solution to Xml and then outputting to Debug and console
let PrintSolution (solution: Solution) =
    let xDoc =
        xdoc [
            xelem "root" [
                (xelem "task" [])   // too lazy to copy initial and required states here.
                (xelem "solution" (
                    match solution with
                    | []    -> [xatt "count" 0]
                    | solution     ->
                        [
                        xatt "count" solution.Length;
                        xatt "steps" (solution.[0].Length-1);
                        solution
                        |> List.mapi ^<|
                            fun i x ->
                                xelem "item" [
                                    xatt "id" i;
                                    XmlFromHistory(x) :> obj
                                ]
                        ]
                    )
                )
            ]
        ]
    xDoc.ToString()
    |> printz

BestSolutions [0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0] [705;330;51;83;812;119;255;109;95;115;89;47;74;94;203;311;73;82;103;741] 401 |> PrintSolution
BestSolutions [1] [5] 5 |> PrintSolution
BestSolutions [1] [5] 0 |> PrintSolution
BestSolutions [0] [5] 0 |> PrintSolution
BestSolutions [0] [5] 5 |> PrintSolution
BestSolutions [0;0] [5;2] 5 |> PrintSolution
BestSolutions [0;0] [5;2] 1 |> PrintSolution
BestSolutions [0;0] [5;2] 3 |> PrintSolution
BestSolutions [0;0;0] [5;7;20] 9 |> PrintSolution
BestSolutions [0;0;0] [5;8;20] 8 |> PrintSolution
BestSolutions [0;0;0] [5;8;20] 9 |> PrintSolution
BestSolutions [0;0;0] [5;8;20] 19 |> PrintSolution
BestSolutions [0;0] [5;8] 2 |> PrintSolution
BestSolutions [0;0] [2;4] 3 |> PrintSolution // impossible
BestSolutions [0;0;0;45;101;0] [408;8;20;439;217;301] 177 |> PrintSolution
